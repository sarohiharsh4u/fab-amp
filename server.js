const express = require('express');
const bodyParser = require('body-parser');
var api=require("./controller/api");


const app = express();


// set the view engine to ejs

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// index page

app.get('/', api.getDetailsPage);


app.listen(8000, function(req,res){

    console.log("Server Started on 8000")

});